package com.example.bcatrinescu.myfirstapp;

import android.net.Uri;
import android.support.v4.widget.CursorAdapter;

/**
 * Created by bcatrinescu on 04.10.2016.
 */

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * {@link ForecastCursorAdapter} exposes a list of weather fragment_forecast
 * from a {@link android.database.Cursor} to a {@link android.widget.ListView}.
 */
public class ForecastCursorAdapter extends CursorAdapter {
    private static final String LOG_TAG = ForecastCursorAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;
//    List<? extends Map<String, ?>> data;
    private boolean mUseTodayLayout;

    public ForecastCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    /** This overridden method will create a View of a certain type and set a ViewHolder object in it's TAG */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose layout type
        int viewType = getItemViewType(cursor.getPosition());
        int layoutId = -1;
        // Determine layoutId from viewType
        switch (viewType) {
            case VIEW_TYPE_TODAY: {
                layoutId = R.layout.fragment_forecasts_list_item_today;
                break;
            }
            case VIEW_TYPE_FUTURE_DAY: {
                layoutId = R.layout.fragment_forecasts_list_item;
                break;
            }
        }
        View v = LayoutInflater.from(context).inflate(layoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        v.setTag(viewHolder);
        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
//        Log.v("ForecastCursorAdapter", "Again, should we display the special TODAY item? " + mUseTodayLayout);
        return (position == 0 && mUseTodayLayout) ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    /* This is where we fill-in the views with the contents of the cursor. */
    @Override
    public void bindView(View v, Context context, Cursor cursor) {
        /** Get the holder object containing each View id we are interested in, part of our layout */
        ViewHolder viewHolder = (ViewHolder) v.getTag();

        String day = cursor.getString(ForecastFragment.COL_WEATHER_NATURAL_DATE);
        (viewHolder.dayTextView).setText(day);

        String iconId = cursor.getString(ForecastFragment.COL_ICON);
        int weatherId = cursor.getInt(ForecastFragment.COL_WEATHER_CONDITION_ID);
        String iconUrlAsString = Utility.getArtUrlForWeatherCondition(context, weatherId, iconId);
//        Log.v(LOG_TAG,"The view icon is " + iconId + " - " + " weather condition " + weatherId + " for " + day);

        Uri forecastImageUri = Uri.parse(iconUrlAsString);
        Picasso.with(context).load(forecastImageUri).into(viewHolder.iconImageView);

        String forecast = cursor.getString(ForecastFragment.COL_WEATHER_DESC);
        (viewHolder.forecastTextView).setText(forecast);
        (viewHolder.forecastTextView).setContentDescription(", will be " + forecast);

//        (viewHolder.iconImageView).setContentDescription("icon " + forecast);

        String highTemp = Utility.formatTemperature(context, cursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP), Utility.isMetric(context));
        (viewHolder.maxTempTextView).setText(highTemp);
        (viewHolder.maxTempTextView).setContentDescription(context.getString(R.string.acc_a_maximum_of, highTemp));

        String lowTemp = Utility.formatTemperature(context, cursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP), Utility.isMetric(context));
        (viewHolder.minTempTextView).setText(lowTemp);
        (viewHolder.minTempTextView).setContentDescription(context.getString(R.string.acc_a_minimum_of, lowTemp));

//        (viewHolder.listItemRootLinearLayout).setTag(R.string.focusedItemTag, cursor.getPosition());
//        // TODO: 28.11.2016 Currently, we can't rotate the screen and keep TalkBack position (highlighted View); to be determined why/how
//        (viewHolder.listItemRootLinearLayout).setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                int position = (int) v.getTag(R.string.focusedItemTag);
////                Log.v(LOG_TAG, "focused item: " + position);
//
//            }
//        });
    }

    public void setUseTodayLayout(boolean dualPane) {
//        Log.v("ForecastCursorAdapter", "the recieved dualPane property is.... " + dualPane);
        if (dualPane == true)
            mUseTodayLayout = false; // don't use the TODAY layout
        else
            mUseTodayLayout = true;
    }

    /** This static object will store the IDs of each of the available views. Use it in a view TAG */
    private static class ViewHolder {
        public final ImageView iconImageView;
        public final TextView dayTextView;
        public final TextView minTempTextView;
        public final TextView maxTempTextView;
        public final TextView forecastTextView;
//        public final LinearLayout listItemRootLinearLayout;

        public ViewHolder(View v){
            iconImageView = (ImageView) v.findViewById(R.id.list_item_icon);
            dayTextView = (TextView) v.findViewById(R.id.list_item_date_textview);
            minTempTextView = (TextView) v.findViewById(R.id.list_item_low_textview);
            maxTempTextView = (TextView) v.findViewById(R.id.list_item_high_textview);
            forecastTextView = (TextView) v.findViewById(R.id.list_item_forecast_textview);
//            listItemRootLinearLayout = (LinearLayout) v.findViewById(R.id.forecast_list_item_root);
        }
    }
}


