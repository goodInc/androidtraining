package com.example.bcatrinescu.myfirstapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bcatrinescu.myfirstapp.data.WeatherContract;
import com.squareup.picasso.Picasso;

import java.util.Date;

import static com.example.bcatrinescu.myfirstapp.DetailFragment.DETAIL_TRANSITION_ANIMATION;
import static com.example.bcatrinescu.myfirstapp.DetailFragment.DETAIL_URI;

public class DetailActivity extends AppCompatActivity {

    private static final String LOG_TAG = DetailActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        // Set our toolbar to represent the "action bar" for this activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();

            // store the required uri for the DetailFragment to use
            Uri uri = getIntent().getData(); // get the uri send by the other activity
            bundle.putParcelable(DETAIL_URI, uri);

            // we are in DetailActivity - which means we are not running in dual-pane mode - so we should always run animations
            bundle.putBoolean(DETAIL_TRANSITION_ANIMATION, true);

            // Create the detail fragment and add it to this activity using a fragment transaction
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.weather_detail_container, detailFragment)
                    .commit();
//            Log.v(LOG_TAG, "Postpone the animation");
            supportPostponeEnterTransition();
        }
    }

}
