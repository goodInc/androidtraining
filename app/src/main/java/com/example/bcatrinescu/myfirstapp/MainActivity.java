package com.example.bcatrinescu.myfirstapp;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.bcatrinescu.myfirstapp.sync.SunshineSyncAdapter;
import com.facebook.stetho.Stetho;
import com.google.android.gms.common.ConnectionResult;

public class MainActivity extends AppCompatActivity {
    /** the final variable below will be used as the KEY in the <key, value> map send to the intent */
    public final static String EXTRA_MESSAGE = "com.example.bcatrinescu.myfirstapp.MESSAGE";
    private static final String LOG_TAG = MainActivity.class.getSimpleName() ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_main);
        // Set our toolbar to represent the "action bar" for this activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SunshineSyncAdapter.initializeSyncAdapter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** Called when the user clicks the "Send" button */
    public void sendMessage(View view){
        Intent intent = new Intent(this,DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.setType("text/plain");
        startActivity(intent);
    }

    /** Called when the user clicks the "Get Weather data" button */
    public void startWeatherActivity(View view){
        Intent intent = new Intent(this, DisplayWeatherActivity.class);
        startActivity(intent);
    }
}