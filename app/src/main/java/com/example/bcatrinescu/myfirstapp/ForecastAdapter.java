package com.example.bcatrinescu.myfirstapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

/**
 * Created by bcatrinescu on 15.12.2016.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {
    private static final String LOG_TAG = ForecastAdapter.class.getSimpleName();
    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;
    private boolean mUseTodayLayout;
    private Cursor mCursor;
    private final Context mContext;
    private final ForecastAdapterOnClickHandler mOnClickHandler;
    private final View mEmptyView;

    public interface ForecastAdapterOnClickHandler {
        void onClick(long date, ForecastAdapter.ViewHolder viewHolder);
    }

    public ForecastAdapter(Context context, View emptyView, ForecastAdapterOnClickHandler onClickHandler) {
        mContext = context;
        mOnClickHandler = onClickHandler;
        mEmptyView = emptyView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ImageView iconImageView;
        public final TextView dayTextView;
        public final TextView minTempTextView;
        public final TextView maxTempTextView;
        public final TextView forecastTextView;

        public ViewHolder(View v) {
            super(v);
            iconImageView = (ImageView) v.findViewById(R.id.list_item_icon);
            dayTextView = (TextView) v.findViewById(R.id.list_item_date_textview);
            minTempTextView = (TextView) v.findViewById(R.id.list_item_low_textview);
            maxTempTextView = (TextView) v.findViewById(R.id.list_item_high_textview);
            forecastTextView = (TextView) v.findViewById(R.id.list_item_forecast_textview);
            itemView.setOnClickListener(this);
        }

        // this method is called when the user selects an item from the RecyclerView
        @Override
        public void onClick(View view) {
            // unlike the ListView pattern, the RecyclerView doesn't "move" the cursor automatically
            mCursor.moveToPosition(getAdapterPosition());
            long selectedDate = mCursor.getLong(ForecastFragment.COL_WEATHER_DATE);
            mOnClickHandler.onClick(selectedDate, this);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 && mUseTodayLayout) ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    // Create new views of a certain type (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (parent instanceof RecyclerView) {
            int layoutId = -1;
            // Determine layoutId from viewType
            switch (viewType) {
                case VIEW_TYPE_TODAY: {
                    layoutId = R.layout.fragment_forecasts_list_item_today;
                    break;
                }
                case VIEW_TYPE_FUTURE_DAY: {
                    layoutId = R.layout.fragment_forecasts_list_item;
                    break;
                }
            }
            View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            v.setFocusable(true);
            return new ViewHolder(v);
        } else {
            throw new RuntimeException("The view is not bound to any RecyclerView");
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (mCursor == null) {
            Log.w(LOG_TAG, "The cursor seems to be null");
            return;
        }
        mCursor.moveToPosition(position);

        String day = mCursor.getString(ForecastFragment.COL_WEATHER_NATURAL_DATE);
        (viewHolder.dayTextView).setText(day);

        String iconId = mCursor.getString(ForecastFragment.COL_ICON);
        int weatherId = mCursor.getInt(ForecastFragment.COL_WEATHER_CONDITION_ID);
        String iconUrlAsString = Utility.getArtUrlForWeatherCondition(mContext, weatherId, iconId);
//        Log.v(LOG_TAG,"The view icon is " + iconId + " - " + " weather condition " + weatherId + " for " + day);

        Uri forecastImageUri = Uri.parse(iconUrlAsString);
        Picasso.with(mContext).load(forecastImageUri).into(viewHolder.iconImageView);

        // setting an unique transaction name so that, in case of the activity loses state (e.g. device rotation)
        // the animator can use this to re-find the original view and it will animate the view back to it's correct position
        ViewCompat.setTransitionName(viewHolder.iconImageView, "iconView" + position);
//        Log.v(LOG_TAG, "The transitionName is: " + ViewCompat.getTransitionName(viewHolder.iconImageView));

        String forecast = mCursor.getString(ForecastFragment.COL_WEATHER_DESC);
        (viewHolder.forecastTextView).setText(forecast);
        (viewHolder.forecastTextView).setContentDescription(", will be " + forecast);

//        (viewHolder.iconImageView).setContentDescription("icon " + forecast);

        String highTemp = Utility.formatTemperature(mContext, mCursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP), Utility.isMetric(mContext));
        (viewHolder.maxTempTextView).setText(highTemp);
        (viewHolder.maxTempTextView).setContentDescription(mContext.getString(R.string.acc_a_maximum_of, highTemp));

        String lowTemp = Utility.formatTemperature(mContext, mCursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP), Utility.isMetric(mContext));
        (viewHolder.minTempTextView).setText(lowTemp);
        (viewHolder.minTempTextView).setContentDescription(mContext.getString(R.string.acc_a_minimum_of, lowTemp));
    }

    // invoked by the layout manager
    @Override
    public int getItemCount() {
        if (mCursor == null) return 0;
        int noOfElements = mCursor.getCount();
        return noOfElements;
    }

    public void setUseTodayLayout(boolean dualPane) {
        if (dualPane == true)
            mUseTodayLayout = false; // as in "don't use the TODAY layout"
        else
            mUseTodayLayout = true;
    }

    public void swapCursor(Cursor newCursor) {
        mCursor = newCursor;
        notifyDataSetChanged();
        if (getItemCount() == 0)
            mEmptyView.setVisibility(View.VISIBLE);
        else
            mEmptyView.setVisibility(View.GONE);
    }

    public Cursor getCursor() {
        return mCursor;
    }
}
