package com.example.bcatrinescu.myfirstapp.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.example.bcatrinescu.myfirstapp.BuildConfig;
import com.example.bcatrinescu.myfirstapp.DisplayWeatherActivity;
import com.example.bcatrinescu.myfirstapp.R;
import com.example.bcatrinescu.myfirstapp.Utility;
import com.example.bcatrinescu.myfirstapp.data.WeatherContract;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static com.example.bcatrinescu.myfirstapp.R.string.pref_weather_units_default;

public class SunshineSyncAdapter extends AbstractThreadedSyncAdapter {
    private final static String LOG_TAG = SunshineSyncAdapter.class.getSimpleName();
    public static final String LOCATION_QUERY_EXTRA = "lqe";
    // Interval at which to sync with the weather, in seconds = 60 seconds * 180 = 3 hours
    public static final int SYNC_INTERVAL = 60 * 180;
    // the sync could be done up to 1 hour BEFORE the time is up
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL/3;
    private final int URL_CONNECT_TIMEOUT_MILS = 2000;
    private final int URL_READ_TIMEOUT_MILS = 3500;
    private static SharedPreferences mSharedPreferences;
    private static String mLocation;
    private static String mNoOfDays;
    private static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
    private static final int WEATHER_NOTIFICATION_ID = 3004;

    private static final String[] NOTIFY_WEATHER_PROJECTION = new String[] {
            WeatherContract.WeatherEntry.COLUMN_WEATHER_CONDITION_ID,
            WeatherContract.WeatherEntry.COLUMN_ICON,
            WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP
    };
    // these indices must match the projection
    private static final int INDEX_WEATHER_CONDITION_ID = 0;
    private static final int INDEX_ICON = 1;
    private static final int INDEX_SHORT_DESC = 2;
    private static final int INDEX_MAX_TEMP = 3;
    private static final int INDEX_MIN_TEMP = 4;

    // Define the list of accepted constant and declare the LocationStatus annotation
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LOCATION_STATUS_OK, LOCATION_STATUS_SERVER_DOWN, LOCATION_STATUS_SERVER_INVALID, LOCATION_STATUS_UNKNOWN, LOCATION_STATUS_INVALID})
    public @interface LocationStatus {}

    // Declare the constants
    public static final int LOCATION_STATUS_OK = 0;
    public static final int LOCATION_STATUS_SERVER_DOWN = 1;
    public static final int LOCATION_STATUS_SERVER_INVALID = 2;
    public static final int LOCATION_STATUS_UNKNOWN = 3;
    public static final int LOCATION_STATUS_INVALID = 4;

    public SunshineSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mLocation = mSharedPreferences.getString(context.getString(R.string.pref_location_key), context.getString(R.string.pref_weather_location_default));
        mNoOfDays = mSharedPreferences.getString(context.getString(R.string.pref_no_of_days_key), context.getString(R.string.pref_no_of_days_default));
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
//        Log.v(LOG_TAG, "+++++++++ onPerformSync Called.");

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Long dateAsLong = calendar.getTimeInMillis();
        Format formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm");
//        Log.v(LOG_TAG, "current date and time:\t " + formatter.format(date));
        provider.getLocalContentProvider().delete(WeatherContract.WeatherEntry.CONTENT_URI, WeatherContract.WeatherEntry.COLUMN_DATE + " <= ?", new String[]{dateAsLong.toString()});
        dateAsLong = null; // mark this eligible for garbage collection
//        Log.v(LOG_TAG, "+++ cleaned up the database of older data");

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;
//            String[] prettyForecastJsonArray = null;
        Vector<ContentValues> prettyForecastJsonVector = null;

//        Log.v(LOG_TAG, "The SyncAdapter background thread seams to be running great, with mLocation: " + mLocation);

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily";
            final String QUERY_PARAM = "q";
            final String FORMAT_PARAM = "mode";
            final String UNITS_PARAM = "units";
            final String DAYS_PARAM = "cnt";
            final String APPID_PARAM = "APPID";
            /** set the default query parameters */
            String mode = "json";
            String units = "metric"; // default value set via the XML in preferences file

            Uri uri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(QUERY_PARAM, mLocation).appendQueryParameter(FORMAT_PARAM, mode)
                    .appendQueryParameter(UNITS_PARAM, units).appendQueryParameter(DAYS_PARAM, mNoOfDays)
                    .appendQueryParameter(APPID_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();
            URL url = new URL(uri.toString());

             /* TEST different server errors by enabling, one by one, the URLs below */
//            URL url = new URL(Uri.parse("https://google.com/?").toString());
//            URL url = new URL(Uri.parse("https://google.com/ping").toString());

            Log.v(LOG_TAG, "Connection URL is: " + url.toString());

//             google photos api
//             https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=park&rankby=prominence&key=
//             https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CoQBcwAAACSwqbux7si-2aoDgBaJVu29eEzP9uYQiAh2vWMLtDCAATq43ZkDZzvxYjj6RvkTCx_Dtfl8FZhn64NG-cQj7LTDuKuXoTiwn26GJqUz-x5rBZOyf5492JTrItFVCNRh4oGyiCorKhYkYrAiH_JQK700O4rqyHT_wlrjMI-fNXymEhAHGZ_Epy9kUoJZ4aU7G5mMGhSppsCTNuJmJnH0zwBetMRkjFs_FQ&key=
//             https://developers.google.com/places/web-service/search#PlaceSearchRequests

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(URL_CONNECT_TIMEOUT_MILS);
            urlConnection.setReadTimeout(URL_READ_TIMEOUT_MILS);
            urlConnection.connect();

            // Catch any status code early, instead of dealing with "FileNotFoundException" for 502 http status code
            final int responseCode = urlConnection.getResponseCode();
            if (responseCode >= HttpURLConnection.HTTP_INTERNAL_ERROR) {
                InputStream inputStream = urlConnection.getErrorStream();
                StringBuffer buffer = new StringBuffer();
                if (buffer != null) {
                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    Log.e(LOG_TAG, "What we've got from the server: ");
                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line + "\n");
                    }
                    if (buffer.indexOf("502") != -1) {
                        Log.e(LOG_TAG, "we got a 502 response status. This is what the server actually returns when you ask for the weather in an non existing city");
                        setLocationStatus(getContext(), LOCATION_STATUS_INVALID);
                    } else {
                        setLocationStatus(getContext(), LOCATION_STATUS_SERVER_DOWN);
                        Log.e(LOG_TAG, String.format("we got a '%s' response from the server\n", buffer.toString()));
                    }
                    reader.close();
                }
                inputStream.close();
                return; // exit early from function
            }

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                setLocationStatus(getContext(), LOCATION_STATUS_SERVER_DOWN);
                // Stream was empty.  No point in parsing.
                return;
            }
            forecastJsonStr = buffer.toString();

//                prettyForecastJsonArray = getWeatherDataFromJson(forecastJsonStr, 7);
//            Log.v(LOG_TAG, "RAW weather data: \n" + forecastJsonStr);
            prettyForecastJsonVector = getWeatherDataFromJson(forecastJsonStr, mLocation, 7, provider);
//            Log.v(LOG_TAG, String.format("+++ We should have weather for %d days to show", prettyForecastJsonVector.size()));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            setLocationStatus(getContext(), LOCATION_STATUS_SERVER_DOWN);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
            return;
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Error ", e);
            setLocationStatus(getContext(), LOCATION_STATUS_SERVER_INVALID);
            // malformed json string or something else. no point in continuing
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        if (prettyForecastJsonVector == null) {
                Log.w(LOG_TAG, "The ARRAY seems to be null!");
            return;
        }

        ContentValues[] contentValuesAsArray = new ContentValues[prettyForecastJsonVector.size()];
        int resultOfDBInsert = provider.getLocalContentProvider().bulkInsert(WeatherContract.WeatherEntry.CONTENT_URI, prettyForecastJsonVector.toArray(contentValuesAsArray));
        Log.v(LOG_TAG, String.format("done updating the data with the SyncAdapter with %d days worth of data", contentValuesAsArray.length));
        setLocationStatus(getContext(), LOCATION_STATUS_OK);
        notifyWeather();
        /** In the end, return the result to display */
//        return prettyForecastJsonVector;
    }

    /**
     * Helper method to have the sync adapter sync immediately
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
//        Log.v(LOG_TAG, "SunshineSyncAdapter syncImmediately");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mLocation = mSharedPreferences.getString(context.getString(R.string.pref_location_key), context.getString(R.string.pref_weather_location_default));
        mNoOfDays = mSharedPreferences.getString(context.getString(R.string.pref_no_of_days_key), context.getString(R.string.pref_no_of_days_default));
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context), context.getString(R.string.content_authority), bundle);
//        Log.v(LOG_TAG, "requestSync called... you should see the onPerformSync method running... any time now...");
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if ( null == accountManager.getPassword(newAccount) ) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                Log.d(LOG_TAG, "getting an accound failed. returning null...");
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
//            Log.v(LOG_TAG, String.format("Configuring periodic sync at %d and flex time at %d",syncInterval, flexTime));
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
//            Log.v(LOG_TAG, String.format("Configuring periodic sync at %d without flex time",syncInterval));
        }
    }


    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        SunshineSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);

        /*
         * Finally, let's do a sync to get things started
         */
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    private void notifyWeather() {
        Context context = getContext();
        //checking the last update and notify if it' the first of the day
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String displayNotificationsKey = context.getString(R.string.pref_weather_notification_key);
        boolean displayNotifications = prefs.getBoolean(displayNotificationsKey, true);
        String lastNotificationKey = context.getString(R.string.pref_notification_last_display_time);
        long lastSync = prefs.getLong(lastNotificationKey, 0);
        String unitsPrefered = mSharedPreferences.getString(getContext().getString(R.string.pref_units_key), getContext().getString(pref_weather_units_default)); // this should not be empty!
        final int notificationID = 1241111;

        if (!displayNotifications) {
                Log.v(LOG_TAG, "I should display a notification, but notifications are disabled");
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationID);
            //reset last sync
            SharedPreferences.Editor editor = prefs.edit();
            editor.putLong(lastNotificationKey, 0);
            editor.apply();
                Log.v(LOG_TAG, "I've also disabled all notifications, if any");
            return;
        }
        boolean isMetric = unitsPrefered.compareTo(getContext().getString(pref_weather_units_default)) == 0 ? true : false; // return "true" if the user set "metric"
        if (System.currentTimeMillis() - lastSync >= DAY_IN_MILLIS) { // as it is it will always display a notification
            // Last sync was more than 1 day ago, let's send a notification with the weather.
            String locationQuery = Utility.getPreferredLocation(context);

            Uri weatherUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(locationQuery, System.currentTimeMillis());

            // we'll query our contentProvider, as always
            Cursor cursor = context.getContentResolver().query(weatherUri, NOTIFY_WEATHER_PROJECTION, null, null, null);

            if (cursor.moveToFirst()) {
                int weatherId = cursor.getInt(INDEX_WEATHER_CONDITION_ID);
                double high = cursor.getDouble(INDEX_MAX_TEMP);
                double low = cursor.getDouble(INDEX_MIN_TEMP);
                String desc = cursor.getString(INDEX_SHORT_DESC);
                String iconId = cursor.getString(INDEX_ICON);

                // Define the text of the forecast.
                String contentText = String.format(context.getString(R.string.format_notification),
                        desc,
                        Utility.formatTemperature(context, high, isMetric),
                        Utility.formatTemperature(context, low, isMetric));

                // Download the icon using Picasso; first prefetch it
                String iconUrlAsString = Utility.getArtUrlForWeatherCondition(getContext(), weatherId, iconId);
                Uri forecastImageUri = Uri.parse(iconUrlAsString);
                Bitmap forecastBitmap = null;
                try {
                    forecastBitmap = Picasso.with(context).load(forecastImageUri).resize(largeIconWidth, largeIconWidth).centerCrop().get();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //build your notification here.
                Date date = new Date();
                Format formatter = new SimpleDateFormat("dd MMMM 'at' HH");

                NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(context);
                mNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
                mNotificationBuilder.setLargeIcon(forecastBitmap);
                mNotificationBuilder.setContentText(contentText);
                mNotificationBuilder.setContentTitle(String.format("%s - %s", mLocation, formatter.format(date)));
                // set the autoCancel property to "true" if you want this to automatically dismiss after the user clicks it
                mNotificationBuilder.setAutoCancel(false);

                // Creates an explicit intent for an Activity in your app
                Intent resultIntent = new Intent(context, DisplayWeatherActivity.class);
                // The stack builder object will contain an artificial back stack for the
                // started Activity.
                // This ensures that navigating backward from the Activity leads out of
                // your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(DisplayWeatherActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                mNotificationBuilder.setContentIntent(pendingIntent);

                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                // notification ID - allows you to update the same notification object where only the "extras" change
                mNotificationManager.notify(notificationID, mNotificationBuilder.build());

                //refreshing last sync
                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(lastNotificationKey, System.currentTimeMillis());
                editor.apply();
            }
        }
    }

    private Vector<ContentValues> getWeatherDataFromJson(String forecastJsonStr, String location, int numDays, ContentProviderClient provider)
            throws JSONException {

        // These are the names of the JSON objects that need to be extracted.
        final String OWM_LIST = "list";
        final String OWM_CITY = "city";
        final String OWM_CITY_NAME = "name";
        final String OWM_COORD = "coord";
        final String OWM_LATITUDE = "lat";
        final String OWM_LONGITUDE = "lon";
        final String OWM_WEATHER = "weather";
        final String OWM_TEMPERATURE = "temp";
        final String OWM_MAX = "max";
        final String OWM_MIN = "min";
        final String OWM_DESCRIPTION = "main";
        final String OWM_ICON = "icon";
        final String OWM_PRESSURE = "pressure";
        final String OWM_HUMIDITY = "humidity";
        final String OWM_WINDSPEED = "speed";
        final String OWM_WIND_DIRECTION = "deg";
        final String OWM_WEATHER_ID = "id";
        final String OWM_MESSAGE_CODE = "cod";

        /** Get the root json object containing all weather and location information */
        JSONObject forecastJson = new JSONObject(forecastJsonStr);

        /** Check if we didn't get the result we want due to bad user input (e.g. unknown location) */
        if (forecastJson.has(OWM_MESSAGE_CODE)){
            int messageCode = forecastJson.getInt(OWM_MESSAGE_CODE);
            switch (messageCode) {
                case HttpURLConnection.HTTP_NOT_FOUND:
                    setLocationStatus(getContext(), LOCATION_STATUS_INVALID);
                    break;
                case HttpURLConnection.HTTP_OK:
                    // nothing to do at this point, as everything seems to be fine
                    break;
                default:
                    setLocationStatus(getContext(), LOCATION_STATUS_SERVER_DOWN);
            }
        } else {
            // if we don't have the "code" node in our JSON, this means the server changed the API
            // and we should check this issue immediately
            setLocationStatus(getContext(), LOCATION_STATUS_SERVER_INVALID);
            return null;
        }
        /** Get the json object containing the location information */
        JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
        String cityName = cityJson.getString(OWM_CITY_NAME);

        JSONObject cityCoord = cityJson.getJSONObject(OWM_COORD);
        String cityLatitude = cityCoord.getString(OWM_LATITUDE);
        String cityLongitude = cityCoord.getString(OWM_LONGITUDE);
//        Log.v(LOG_TAG, String.format("LAT %s LON %s", cityLatitude, cityLongitude));
        long locationId = addLocation(location, cityName, cityLatitude, cityLongitude, provider);

        /** Get the object containing only the weather information for the number of days requested */
        JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);

        // OWM returns daily fragment_forecast based upon the local time of the city that is being
        // asked for, which means that we need to know the GMT offset to translate this data
        // properly.

        // Since this data is also sent in-order and the first day is always the
        // current day, we're going to take advantage of that to get a nice
        // normalized UTC date for all of our weather.
//
//            Time dayTime = new Time();
//            dayTime.setToNow();
//
//            // we start at the day returned by local time. Otherwise this is a mess.
//            int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);
//
//            // now we work exclusively in UTC
//            dayTime = new Time();

//        List<Map<String, String>> resultListOfDaysAsMaps = new ArrayList<>();

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        SimpleDateFormat.getDateInstance();
        SimpleDateFormat formatterToday = ((SimpleDateFormat) DateFormat.getDateInstance()); // TODO: 28.11.2016  CAST MUST BE in TRY-CATCH
        formatterToday.applyPattern("MMMM d");
        SimpleDateFormat formatterWeekDay = ((SimpleDateFormat) DateFormat.getDateInstance()); // TODO: 28.11.2016  CAST MUST BE in TRY-CATCH
        formatterWeekDay.applyPattern("EEEE");
        SimpleDateFormat formatterExactDay = ((SimpleDateFormat) DateFormat.getDateInstance()); // TODO: 28.11.2016  CAST MUST BE in TRY-CATCH
        formatterExactDay.applyPattern("EEE, MMM d");


        Vector<ContentValues> contentValuesVector = new Vector<>(weatherArray.length());

        for (int i = 0; i < weatherArray.length(); i++, calendar.add(Calendar.DATE, 1)) {
            String day;
            String description;
            String icon;
            long dateTime = calendar.getTimeInMillis();
            double pressure;
            int humidity;
            double windSpeed;
            double windDirection;
            int weatherId;

            // Get the JSON object representing the day
            JSONObject dayForecast = weatherArray.getJSONObject(i);
            /** Prepare the map for the SimpleAdapter; this will represent a "row" with a date */
            Map<String, String> resultMap = new HashMap<>();

            if (i == 0) {
                day = getContext().getString(R.string.loc_today, formatterToday.format(calendar.getTime()));
            } else if (i == 1) {
                day = getContext().getString(R.string.loc_tomorrow);
            } else if (i < 7) {
                day = formatterWeekDay.format(calendar.getTime());
            } else {
                day = formatterExactDay.format(calendar.getTime());
            }

            // description is in a child array called "weather", which is 1 element long.
            JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
            description = weatherObject.getString(OWM_DESCRIPTION);
            icon = weatherObject.getString(OWM_ICON);
//                Log.v(LOG_TAG,"The data icon is: " + icon);

            // Temperatures are in a child object called "temp".  Try not to name variables
            // "temp" when working with temperature.  It confuses everybody.
            JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);

            weatherId = weatherObject.getInt(OWM_WEATHER_ID);
            double high = temperatureObject.getDouble(OWM_MAX);
            double low = temperatureObject.getDouble(OWM_MIN);
            Log.v(LOG_TAG, String.format("\t\t%s\t\tmax %.1f min %.1f \t%s", location, high, low, day));

            /** format the temperatures, from Metric to anything you want (currently, Imperial is supported) */
            /** we assume that all data is metric and we convert it on the fly */
            String unitsPrefered = mSharedPreferences.getString(getContext().getString(R.string.pref_units_key), getContext().getString(R.string.pref_weather_units_default)); // this should not be empty!
            boolean isMetric = unitsPrefered.compareTo(getContext().getString(pref_weather_units_default)) == 0 ? true : false; // return "true" if the user set "metric"

            String formattedHighTemp = Utility.formatTemperature(getContext(), high, isMetric);
            String formattedLowTemp = Utility.formatTemperature(getContext(), low, isMetric);

            pressure = dayForecast.getDouble(OWM_PRESSURE);
            humidity = dayForecast.getInt(OWM_HUMIDITY);
            windSpeed = dayForecast.getDouble(OWM_WINDSPEED);
            windDirection = dayForecast.getDouble(OWM_WIND_DIRECTION);

            ContentValues weatherValues = new ContentValues();

            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_LOC_KEY, locationId);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DATE, dateTime);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_NATURAL_DATE, day);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, windDirection);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, high);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, low);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC, description);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_CONDITION_ID, weatherId);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_ICON, icon);

            contentValuesVector.add(weatherValues);
//
//            resultMap.put("list_item_date_textview", day);
//            resultMap.put("list_item_forecast_textview", description);
//            /** don't hardcode the formatting as below ! instead use a format defined as a string, get it from the context etc... */
//                /*resultMap.put("list_item_high_textview", Long.toString(highLowResult[0]) + " " + unitsDegree);*/
//            resultMap.put("list_item_high_textview", formattedHighTemp);
//            resultMap.put("list_item_low_textview", formattedLowTemp);
//
//                /* Integer.toString(R.drawable.clouds_and_sun)  */
////                RequestCreator rc = Picasso.with(getContext()).load("http://openweathermap.org/img/w/02n.png").placeholder(R.drawable.clouds_and_sun);
//
//
//            resultMap.put("list_item_icon", icon);
//            resultListOfDaysAsMaps.add(resultMap);
        }
        return contentValuesVector;
    }

    long addLocation(String locationSetting, String cityName, String lat, String lon, ContentProviderClient provider) {
        // Students: First, check if the location with this city name exists in the db
        // If it exists, return the current ID
        // Otherwise, insert it using the content resolver and the base URI

        ContentValues contentValues = new ContentValues();
        contentValues.put(WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,locationSetting);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_CITY_NAME,cityName);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, lat);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, lon);

        // A cursor is your primary interface to the query results.
        Cursor cursor = provider.getLocalContentProvider().query(
                WeatherContract.LocationEntry.CONTENT_URI,
                null,
                WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + "=?",
                new String[]{locationSetting},
                null);


        if (cursor.moveToFirst() == false) {
            Uri uri = provider.getLocalContentProvider().insert(WeatherContract.LocationEntry.CONTENT_URI, contentValues);
//            Log.v(LOG_TAG, "+++++ addLocation called with location id and name: " + Long.parseLong(uri.getLastPathSegment()) + " " + locationSetting + " new LOCATION inserted");
            return Long.parseLong(uri.getLastPathSegment());
        } else {
//            Log.v(LOG_TAG, "+++++ addLocation called with location id and name: " + cursor.getLong(cursor.getColumnIndex(WeatherContract.LocationEntry._ID)) + " " + locationSetting + " LOCATION already exists");
            return cursor.getLong(cursor.getColumnIndex(WeatherContract.LocationEntry._ID));
        }
    }

    private void setLocationStatus(Context context, @LocationStatus int status) {
        // Use 'commit' instead of 'apply' when you edit your SharedPreferences in a BACKGROUND thread; use 'apply' while saving stuff in a foreground thread
        mSharedPreferences.edit().putInt(context.getString(R.string.pref_location_status_key), status).commit();
    }

    int largeIconWidth = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB
            ? Resources.getSystem().getDimensionPixelSize(android.R.dimen.notification_large_icon_width)
            : Resources.getSystem().getDimensionPixelSize(R.dimen.notification_large_icon_default);
}