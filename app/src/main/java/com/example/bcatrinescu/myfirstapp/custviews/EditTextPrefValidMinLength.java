package com.example.bcatrinescu.myfirstapp.custviews;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.example.bcatrinescu.myfirstapp.R;

/**
 * Created by bcatrinescu on 17.11.2016.
 */

public class EditTextPrefValidMinLength extends EditTextPreference {
    private static final String LOG_TAG = EditTextPrefValidMinLength.class.getSimpleName();
    // setting this to a different value than the one specified in the XML where the custom view is used,
    // allows us to test that the components function correctly
    private static final int DEFAULT_MINIMUM_LOCATION_LENGTH = 2;
    private int mMinLength;

    public EditTextPrefValidMinLength(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EditTextPrefValidMinLength, 0, 0);
        try {
            // set a minimum length of 3 characters for our weather location
            mMinLength = a.getInteger(R.styleable.EditTextPrefValidMinLength_minLength, DEFAULT_MINIMUM_LOCATION_LENGTH);
//            Log.v(LOG_TAG, "The minimum length of our weather location should be: " + mMinLength);
        } finally {
            a.recycle();
        }
    }

    public int getMinLength() {
        return mMinLength;
    }

    public void setMinLength(int minLength) {
        this.mMinLength = minLength;
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        final Dialog dialog = getDialog();
        final String originalTitle = getTitle().toString();
        if (dialog instanceof AlertDialog) {
            EditText editText = getEditText();
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > DEFAULT_MINIMUM_LOCATION_LENGTH) {
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    } else {
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }
}
