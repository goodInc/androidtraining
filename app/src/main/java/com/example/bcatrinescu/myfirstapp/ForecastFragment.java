package com.example.bcatrinescu.myfirstapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bcatrinescu.myfirstapp.data.WeatherContract;
import com.example.bcatrinescu.myfirstapp.sync.SunshineSyncAdapter;
import com.squareup.picasso.Picasso;

import java.util.Date;

import static com.example.bcatrinescu.myfirstapp.Utility.getLocationStatus;
import static com.example.bcatrinescu.myfirstapp.Utility.getNetworkConnectivityStatus;

public class ForecastFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 27451927;
    private static final int FORECAST_LOADER = 1412122;
    private static final String LOG_TAG = ForecastFragment.class.getSimpleName();
    private static TextView sForecastEmptyListView;
    private View mParallaxView;
    private AppBarLayout mAppBarView;
    public SharedPreferences sharedPreferences;
    private int mCurCheckPosition;
    public int mFocusedItemId;
    private boolean mDualPane;
    private long mCurrentDate;
    public ForecastAdapter mForecastAdapter;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean mHoldForTransition;

    @Override
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(getContext()).unregisterOnSharedPreferenceChangeListener(this);
    }

    public static final String[] FORECAST_COLUMNS = {
            // In this case the id needs to be fully qualified with a table name, since
            // the content provider joins the location & weather tables in the background
            // (both have an _id column)
            // On the one hand, that's annoying.  On the other, you can search the weather table
            // using the location set by the user, which is only in the Location table.
            // So the convenience is worth it.
            WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
            WeatherContract.WeatherEntry.COLUMN_ICON,
            WeatherContract.WeatherEntry.COLUMN_DATE,
            WeatherContract.WeatherEntry.COLUMN_NATURAL_DATE,
            WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
            WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,
            WeatherContract.WeatherEntry.COLUMN_WEATHER_CONDITION_ID,
            WeatherContract.LocationEntry.COLUMN_COORD_LAT,
            WeatherContract.LocationEntry.COLUMN_COORD_LONG,
            WeatherContract.WeatherEntry.COLUMN_HUMIDITY,
            WeatherContract.WeatherEntry.COLUMN_PRESSURE,
            WeatherContract.WeatherEntry.COLUMN_WIND_SPEED,
            WeatherContract.WeatherEntry.COLUMN_DEGREES
    };


    // These indices are tied to FORECAST_COLUMNS.  If FORECAST_COLUMNS changes, these
    // must change.
    public static final int COL_WEATHER_ID = 0;
    public static final int COL_ICON = 1;
    public static final int COL_WEATHER_DATE = 2;
    public static final int COL_WEATHER_NATURAL_DATE = 3;
    public static final int COL_WEATHER_DESC = 4;
    public static final int COL_WEATHER_MAX_TEMP = 5;
    public static final int COL_WEATHER_MIN_TEMP = 6;
    public static final int COL_LOCATION_SETTING = 7;
    public static final int COL_WEATHER_CONDITION_ID = 8;
    public static final int COL_COORD_LAT = 9;
    public static final int COL_COORD_LONG = 10;
    public static final int COL_HUMIDITY = 11;
    public static final int COL_PRESSURE = 12;
    public static final int COL_WIND_SPEED = 13;
    public static final int COL_WIND_DEGREES = 14;

    public ForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", mCurCheckPosition);
        // TODO: 02.12.2016 Implement accessibility features (focus issues)
//        FragmentActivity fragmentActivity = getActivity();
//        Log.v(LOG_TAG, "fragmentActivity of type: " + fragmentActivity.getClass().getSimpleName());
//        if (fragmentActivity != null) {
//            View view = fragmentActivity.getCurrentFocus();
//            if (view != null) {
//                Log.v(LOG_TAG, "Persisting view to be focused with id: " + view.getId() + " and type " + view.getClass().getSimpleName());
//                outState.putInt("focusedItemId", view.getId());
//            } else {
//                Log.v(LOG_TAG, "No view has focus!");
//            }
//        } else {
//            Log.v(LOG_TAG, "The ForecastFragment seems to be attached to a Context");
//        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (mHoldForTransition) {
            getActivity().supportPostponeEnterTransition();
//            Log.v(LOG_TAG, "Postpone the enter transition!");
        }
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(FORECAST_LOADER, null, this);

        // Check to see if we have a frame in which to embed the details
        // fragment directly in the containing UI.
        View detailsFrame = getActivity().findViewById(R.id.weather_detail_container);
        mParallaxView = getActivity().findViewById(R.id.parallax_bar);
        mAppBarView = (AppBarLayout) getActivity().findViewById(R.id.portrait_app_bar);
//        Log.v(LOG_TAG, "App bar is: " + mAppBarView == null? "null": "NOT null");
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;
        mForecastAdapter.setUseTodayLayout(mDualPane);

//        Log.v(LOG_TAG, "Are we in DUAL-PANE mode? " + mDualPane);

        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
            /*mRecyclerView.setItemChecked(mCurCheckPosition, true);*/

            // TODO: 02.12.2016 to implement accessibility features (focus issues...)
//            mFocusedItemId = savedInstanceState.getInt("focusedItemId", 0);
//            if (mFocusedItemId != 0) {
//                boolean itemWasFocusedResult = getActivity().findViewById(mFocusedItemId).requestFocus();
//                if (itemWasFocusedResult)
//                    Log.v(LOG_TAG, "Item was focused succesfully with id: " + mFocusedItemId);
//                else
//                    Log.v(LOG_TAG, "Item was not focused :( with id: " + mFocusedItemId);
//            }
        }
        if (mDualPane) {
            // In dual-pane mode, the list view highlights the selected item.
            /*mRecyclerView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);*/
            // Make sure our UI is in the correct state.
            /*mRecyclerView.setItemChecked(mCurCheckPosition, true);*/
//            Log.v(LOG_TAG, mCurCheckPosition + " member position should be checked!!! " + " and dual pane is: " + mDualPane);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateWeather();
    }

    @Override
    public void onResume() {
        super.onResume();
        /** The reason you have to cast is because getActivity() returns a FragmentActivity and you need an AppCompatActivity */
//        ((AppCompatActivity) getActivity()).getSupportActionBar()
//                .setTitle(sharedPreferences.getString(
//                        getString(R.string.pref_location_key),
//                        getString(R.string.pref_weather_location_default))
//                );
        /** Set the title of the Toolbar to be the last set location */
//        ((Toolbar) getActivity().findViewById(R.id.weathertoolbar))
//                .setTitle(sharedPreferences.getString(getString(R.string.pref_weather_location_key), getString(R.string.pref_weather_location_default)));
        PreferenceManager.getDefaultSharedPreferences(getContext()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
//        Log.v(LOG_TAG, "onInflate called");
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ForecastFragment, 0, 0);
        try {
            // this throws a Runtime exception if the array was recycled
            mHoldForTransition = a.getBoolean(R.styleable.ForecastFragment_sharedElementTransitions, false);
        } finally {
            a.recycle();
        }
//        Log.v(LOG_TAG, "Should we hold for transitions? " + mHoldForTransition);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FrameLayout rootView = (FrameLayout) inflater.inflate(R.layout.fragment_forecasts, container, false);
//        /** plain text list adapter */
//        mForecastCursorAdapter = new ArrayAdapter<>(getContext(), R.layout.simple_list_item_activated_1, new ArrayList<String>());
//        rootView.setAdapter(mForecastCursorAdapter);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_forecast);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        sForecastEmptyListView = (TextView) rootView.findViewById(R.id.recyclerview_forecast_empty);
        mForecastAdapter = new ForecastAdapter(getContext(), sForecastEmptyListView, new ForecastAdapter.ForecastAdapterOnClickHandler() {
            @Override
            public void onClick(long date, ForecastAdapter.ViewHolder viewHolder) {
                mCurCheckPosition = viewHolder.getAdapterPosition();
                String locationSetting = Utility.getPreferredLocation(getActivity());
                Uri uri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(locationSetting, date);
                FragmentActivity fragmentActivity = (FragmentActivity) getContext();
                if (!(fragmentActivity instanceof ForecastFragment.Callback)) {
                    throw new RuntimeException("The parent activity doesn't implement the Callback interface");
                } else {
                    ((ForecastFragment.Callback) fragmentActivity).onItemSelected(uri, viewHolder);
                }
            }
        });

        mRecyclerView.setAdapter(mForecastAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        if (null != mParallaxView) {
                            int max = mParallaxView.getHeight();
//                            Log.v(LOG_TAG, "Parallax height is: " + max + " with translation of " + mParallaxView.getTranslationY() + " and dy " + dy);
                            if (dy > 0) {
                                mParallaxView.setTranslationY(Math.max(-max, mParallaxView.getTranslationY() - dy / 2));
                            } else {
                                mParallaxView.setTranslationY(Math.min(0, mParallaxView.getTranslationY() - dy / 2));
                            }
                        }
                    }
                });
            }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (null != mAppBarView) {
                        /* this will allow you to tell how much the recycleview has scrolled */
                        int scrollY = mRecyclerView.computeVerticalScrollOffset();
//                        Log.v(LOG_TAG, "RecyclerView, given the orig position, scrolled: " + scrollY);
                        if (scrollY <= 0 ) {
                            mAppBarView.setElevation(0f);
                        } else {
                            // it seems that floating points like 0.5 are not accepted. it must be a "round" value
                            mAppBarView.setElevation(getResources().getDimensionPixelSize(R.dimen.appbar_elevation));
                        }
                    }
                }
            });
        }

        /*mRecyclerView.setEmptyView(sForecastEmptyListView);*/
        /*mRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                mCurCheckPosition = position;
                if (mDualPane) {
                    mRecyclerView.setItemChecked(position, true);
//                    Log.v(LOG_TAG, position + " position should be checked!!! " + " and dual pane is: " + mDualPane);
                }
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(mCurCheckPosition);
                Uri uri = null;
                String locationSetting = Utility.getPreferredLocation(getActivity());
                if (cursor != null) {
                    mCurrentDate = cursor.getLong(COL_WEATHER_DATE);
                    uri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(
                            locationSetting, mCurrentDate);
                }
                FragmentActivity fragmentActivity = getActivity();
                if (!(fragmentActivity instanceof Callback)) {
                    throw new RuntimeException("The parent activity doesn't implement the Callback interface");
                } else {
                    ((Callback) fragmentActivity).onItemSelected(uri);
                }
            }
        });*/
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forecast_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.action_refresh) {
//            // check if you still have the INTERNET permission
//            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.INTERNET) !=
//                    PackageManager.PERMISSION_GRANTED) {
//
//                // check whatever we should present an explanation
//                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.INTERNET)) {
//                    // explain to the user WHY we need this permission *asynchronously*
//                } else {
//                    // no explanation needed, we can request the permission
//                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.INTERNET}, MY_PERMISSIONS_REQUEST_INTERNET);
//                    // MY_PERMISSIONS_REQUEST_INTERNET is an app-defined int constant
//                    // the callback gets the result of the request
//                }
//            } else {
//                updateWeather();
//            }
//        }
        if (id == R.id.action_view_location_on_map && null != mForecastAdapter) {
            String preferredLocation = sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_weather_location_default));
            Cursor c = mForecastAdapter.getCursor();
            if ( null != c ) {
                c.moveToPosition(0);
                String posLat = c.getString(COL_COORD_LAT);
                String posLong = c.getString(COL_COORD_LONG);
//                Log.v(LOG_TAG, String.format("the latitude and longitude for %s is %s and %s", preferredLocation, posLat, posLong));
                final int zoomLevel = 8;
                /** The Google search results pin point the location better on the map without the coordinates
                 * However, it is best to use them to avoid any discrepancy between the weather service and Google maps
                 * For example, "Poitiers" is in France and Romania and the weather service could pick the one in France
                 * and Google could pick the one in Romania */
                Uri mapsIntentUri = Uri.parse(String.format("geo:%s,%s?z=%d&q=%s", posLat, posLong, zoomLevel, Uri.encode(preferredLocation)));
//                Log.v(LOG_TAG, "Complete URI for Google Maps is: " + mapsIntentUri.toString());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapsIntentUri);
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // send that intent then!
                    startActivity(mapIntent);
                } else {
                    // inform the user that there is no maps application
                    Toast.makeText(getContext(), "No \"maps\" application found!", Toast.LENGTH_LONG).show();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateWeather() {
        String location = sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_weather_location_default));
//        /** start a new thread (AsyncTask) to update the data in the database */
////        new FetchWeatherTask(getContext(), this).execute(location);
//        /** start a background service to update the data in the database */
////        Intent sunshineServiceIntent = new Intent(getContext(), SunshineService.class);
////        sunshineServiceIntent.putExtra("location", location);
////        getContext().startService(sunshineServiceIntent);
////        getLoaderManager().restartLoader(FORECAST_LOADER, null, this);
        // the AlarmManager is a system service
//        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
//        Log.v(LOG_TAG, "Update Weather method called with location: " + location);
//        // create an Intent and, later on, wrap it with a Pending Intent
//        Intent intent = new Intent(getContext(), SunshineService.AlarmReceiver.class);
//        intent.putExtra(SunshineService.LOCATION_QUERY_EXTRA, location);
//        // the PendingIntent must be flagged for an one time use - otherwise you'll get the same PendingIntent
//        // when trying to create a new one (they differ by Intent.filters, not by "extras"
//        PendingIntent alarmIntent = PendingIntent.getBroadcast(getContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 1000 * 3, alarmIntent);
        //SunshineSyncAdapter.syncImmediately(getActivity());
        getLoaderManager().restartLoader(FORECAST_LOADER, null, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                // if the request is canceled, the result arrays are empty
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // do your thing
                    updateWeather();
                } else {
                    // permission denied. Disable the functionality that depends on this permission
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might expect
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (isAdded()) {
            if (key.equals(getString(R.string.pref_location_key))) {
//                ((Toolbar) getActivity().findViewById(R.id.weathertoolbar))
//                        .setTitle(sharedPreferences.getString(getString(R.string.pref_weather_location_key), ""));
//                updateWeather(); // don't need this any more - the app will update the weather when the Fragment is started (onStart)
            } else if (key.equals(getString(R.string.pref_units_key))) {
//                updateWeather(); // don't need this any more - the app will update the weather when the Fragment is started (onStart)
            } else if (key.equals(getString(R.string.pref_location_status_key))) {
                updateEmptyView();
            }
        }
    }

    private void updateEmptyView() {
        @SunshineSyncAdapter.LocationStatus int locationStatus = getLocationStatus(getContext());
        int message = R.string.empty_forecast_list;
        switch (locationStatus) {
            case SunshineSyncAdapter.LOCATION_STATUS_SERVER_DOWN:
                message = R.string.empty_forecast_list_server_down;
                break;
            case SunshineSyncAdapter.LOCATION_STATUS_SERVER_INVALID:
                message = R.string.empty_forecast_list_server_error;
                break;
            case SunshineSyncAdapter.LOCATION_STATUS_INVALID:
                message = R.string.empty_forecast_list_invalid_location;
                break;
            default: {
                if (!getNetworkConnectivityStatus(getContext()))
                    message = R.string.empty_forecast_no_internet_connection;
            }
        }
        sForecastEmptyListView.setText(message);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        String location = sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_weather_location_default));
        return new CursorLoader(
                getContext(),
                WeatherContract.WeatherEntry.buildWeatherLocationWithStartDate(location, System.currentTimeMillis()),
                FORECAST_COLUMNS,
                null,
                null,
                WeatherContract.WeatherEntry.COLUMN_DATE + " ASC"
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        if (!cursor.moveToFirst()) {
            updateEmptyView();
        }
        mForecastAdapter.swapCursor(cursor);
        if ( cursor.getCount() == 0 ) {
            getActivity().supportStartPostponedEnterTransition();
//            Log.v(LOG_TAG, "Start postponed transition");
        } else {
            mRecyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                /**
                 * The listener below is HIGHLY important!! It allows us to wait until all the views in the tree
                 * have been measured and given a frame.
                 * Only at this point we know for sure that an unique 'transactionName' has been set programmatically
                 * in the ForecastAdapter for each shared element!!! (1.3 days)
                 *
                 * As a side note, from the documentation: 'clients can use this to adjust their scroll bounds
                 * or even to request a new layout before drawing occurs.
                 * Return true to proceed with the current drawing pass, or false to cancel.'
                 *
                 * **/
                @Override
                public boolean onPreDraw() {
                    // Since we know we're going to get items, we keep the listener around until
                    // we see Children.
                    if (mRecyclerView.getChildCount() > 0) {
                        mRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
//                        int position = mForecastAdapter.getSelectedItemPosition();
//                        if (position == RecyclerView.NO_POSITION &&
//                                -1 != mInitialSelectedDate) {
//                            Cursor data = mForecastAdapter.getCursor();
//                            int count = data.getCount();
//                            int dateColumn = data.getColumnIndex(WeatherContract.WeatherEntry.COLUMN_DATE);
//                            for ( int i = 0; i < count; i++ ) {
//                                data.moveToPosition(i);
//                                if ( data.getLong(dateColumn) == mInitialSelectedDate ) {
//                                    position = i;
//                                    break;
//                                }
//                            }
//                        }
//                        if (position == RecyclerView.NO_POSITION) position = 0;
//                        // If we don't need to restart the loader, and there's a desired position to restore
//                        // to, do so now.
//                        mRecyclerView.smoothScrollToPosition(position);
//                        RecyclerView.ViewHolder vh = mRecyclerView.findViewHolderForAdapterPosition(position);
//                        if (null != vh && mAutoSelectView) {
//                            mForecastAdapter.selectView(vh);
//                        }
                        if ( mHoldForTransition ) {
                            getActivity().supportStartPostponedEnterTransition();
//                            Log.v(LOG_TAG, "Start postponed transition");
                        }
                        return true;
                    }
                    return false;
                }
            });
        }
        /*mRecyclerView.setItemChecked(mCurCheckPosition, true);*/
        /* Do we need to scroll into position? */
//        mRecyclerView.smoothScrollToPosition(mCurCheckPosition);
    }


    public long getCurrentDateAsLong() {
        return mCurrentDate != 0 ? mCurrentDate : new Date().getTime();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.
        mForecastAdapter.swapCursor(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mRecyclerView) {
            mRecyclerView.clearOnScrollListeners();
        }
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callback {
        /**
         * DetailFragmentCallback for when an item has been selected.
         */
        public void onItemSelected(Uri dateUri, ForecastAdapter.ViewHolder vh);
    }
}