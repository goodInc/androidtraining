package com.example.bcatrinescu.myfirstapp;

/**
 * Created by bcatrinescu on 04.10.2016.
 */

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.example.bcatrinescu.myfirstapp.sync.SunshineSyncAdapter;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

public class Utility {
    public static String getPreferredLocation(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String location = prefs.getString(context.getString(R.string.pref_location_key), context.getString(R.string.pref_weather_location_default));
        return location;
    }

    public static boolean isMetric(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String unitsPrefered = prefs.getString(context.getString(R.string.pref_units_key), context.getString(R.string.pref_weather_units_default)); // this should not be empty!
        boolean isMetric = unitsPrefered.compareTo(context.getString(R.string.pref_weather_units_default)) == 0 ? true : false; // return "true" if the user set "metric"
        return isMetric;
    }

    /**
     * Prepare the weather high/lows for presentation.
     */
    private static String formatHighLows(Context context, double high, double low) {
        boolean isMetric = isMetric(context);
        String highLowStr = formatTemperature(context, high, isMetric) + "/" + Utility.formatTemperature(context, low, isMetric);
        return highLowStr;
    }

    public static String formatTemperature(Context context, double temperature, boolean isMetric) {
        double temp;
        if (isMetric == false) {
            temp = (temperature * 1.8) + 32;
        } else {
            temp = temperature;
        }
        /** best practice! use a pre-defined format instead of hardcoding it in the code! */
        /** The numbers will be formatted using the device locale into account; e.g. it could say 3.4 or 3,4 */
        return context.getString(R.string.format_temperature, temp);
    }

    static String formatDate(long dateInMillis) {
        Date date = new Date(dateInMillis);
        return DateFormat.getDateInstance().format(date);
    }

    static public boolean getNetworkConnectivityStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @SuppressWarnings("ResourceType")
    static public @SunshineSyncAdapter.LocationStatus int getLocationStatus(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(context.getString(R.string.pref_location_status_key), SunshineSyncAdapter.LOCATION_STATUS_UNKNOWN);
    }

    static public void resetLocationStatus(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        // use 'apply' since it will be used from the UI thread (immediate in-memory and async on the disk/storage)
        preferences.edit().putInt(context.getString(R.string.pref_location_status_key),SunshineSyncAdapter.LOCATION_STATUS_UNKNOWN).apply();
    }

    static public void resetNotificationStatus(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String lastNotificationKey = context.getString(R.string.pref_notification_last_display_time);
        //reset last sync
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(lastNotificationKey, 0);
        editor.apply();
    }

    /**
     * Helper method to provide the art urls according to the weather condition id returned
     * by the OpenWeatherMap call.
     *
     * @param context Context to use for retrieving the URL format
     * @param weatherId from OpenWeatherMap API response
     * @return url for the corresponding weather artwork. null if no relation is found.
     */
    public static String getArtUrlForWeatherCondition(Context context, int weatherId, String iconId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String formatArtUrl = prefs.getString(context.getString(R.string.pref_art_pack_key), context.getString(R.string.pref_art_pack_original));

        if (!formatArtUrl.equals(context.getString(R.string.pref_art_pack_original))) {
            if (weatherId >= 200 && weatherId <= 232) {
                return String.format(formatArtUrl, "storm");
            } else if (weatherId >= 300 && weatherId <= 321) {
                return String.format(formatArtUrl, "light_rain");
            } else if (weatherId >= 500 && weatherId <= 504) {
                return String.format(formatArtUrl, "rain");
            } else if (weatherId == 511) {
                return String.format(formatArtUrl, "snow");
            } else if (weatherId >= 520 && weatherId <= 531) {
                return String.format(formatArtUrl, "rain");
            } else if (weatherId >= 600 && weatherId <= 622) {
                return String.format(formatArtUrl, "snow");
            } else if (weatherId >= 701 && weatherId <= 761) {
                return String.format(formatArtUrl, "fog");
            } else if (weatherId == 761 || weatherId == 781) {
                return String.format(formatArtUrl, "storm");
            } else if (weatherId == 800) {
                return String.format(formatArtUrl, "clear");
            } else if (weatherId == 801) {
                return String.format(formatArtUrl, "light_clouds");
            } else if (weatherId >= 802 && weatherId <= 804) {
                return String.format(formatArtUrl, "clouds");
            }
        }
        return String.format(formatArtUrl, iconId);
    }
}