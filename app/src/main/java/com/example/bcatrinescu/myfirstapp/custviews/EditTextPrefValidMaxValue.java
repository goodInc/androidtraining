package com.example.bcatrinescu.myfirstapp.custviews;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import com.example.bcatrinescu.myfirstapp.R;

/**
 * Created by bcatrinescu on 10.01.2017.
 */

public class EditTextPrefValidMaxValue extends EditTextPreference {
    private static final String LOG_TAG = EditTextPrefValidMaxValue.class.getSimpleName();
    // setting this to a different value than the one specified in the XML where the custom view is used,
    // allows us to test that the components function correctly
    private static final int DEFAULT_MAXIMUM_NUMERIC_VALUE = 14;
    private int mMaxValue;

    public EditTextPrefValidMaxValue(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EditTextPrefValidMaxValue, 0, 0);
        try {
            // set a maximum value of 14 'days' for our weather location
            mMaxValue = a.getInteger(R.styleable.EditTextPrefValidMaxValue_maxValue, 14);
        } finally {
            a.recycle();
        }
    }

    public int getmMaxValue() {
        return mMaxValue;
    }

    public void setmMaxValue(int mMaxValue) {
        this.mMaxValue = mMaxValue;
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        final Dialog dialog = getDialog();
        if (dialog instanceof AlertDialog) {
            EditText editText = getEditText();
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        int inputValue = Integer.parseInt(s.toString());
                        Log.v(LOG_TAG, "Days worth of data: " + inputValue);
                        if (0 < inputValue && inputValue <= DEFAULT_MAXIMUM_NUMERIC_VALUE) {
                            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        } else {
                            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        }
                    } catch (NumberFormatException nfe) {
                        // should also inform the user why the input is not working
                        Log.w(LOG_TAG, "Can't tell if this is a number");
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }
}
