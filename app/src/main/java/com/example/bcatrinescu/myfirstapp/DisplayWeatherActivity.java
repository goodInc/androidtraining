package com.example.bcatrinescu.myfirstapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.bcatrinescu.myfirstapp.data.WeatherContract;

import java.util.Date;

import static com.example.bcatrinescu.myfirstapp.DetailFragment.DETAIL_TRANSITION_ANIMATION;
import static com.example.bcatrinescu.myfirstapp.DetailFragment.DETAIL_URI;

public class DisplayWeatherActivity extends AppCompatActivity implements ForecastFragment.Callback {

    private static final String DETAILFRAGMENT_TAG = "DFTAG";
    private static final String LOG_TAG = DisplayWeatherActivity.class.getSimpleName();
    private boolean mTwoPane;
    private int mCurCheckPosition = 0;
    private String mLocation;
    private long mCurentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_weather);
        // Set our toolbar to represent the "action bar" for this activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        ForecastFragment forecastFragment = new ForecastFragment();
//        getSupportFragmentManager().beginTransaction().add(R.id.fragment_forecast, forecastFragment);
//
        mLocation = Utility.getPreferredLocation(this);

        if (findViewById(R.id.weather_detail_container) != null) {
            // The detail container view will be present only in the large-screen layouts
            // (res/layout-sw600dp). If this view is present, then the activity should be
            // in two-pane mode.
            mTwoPane = true;
//            Log.v(LOG_TAG, "We are in TWO PANE mode");
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            Bundle bundle = new Bundle();

            // store the required uri for the DetailFragment to use
            Uri uri =  WeatherContract.WeatherEntry.buildWeatherLocationWithStartDate(mLocation, new Date().getTime());
            bundle.putParcelable(DETAIL_URI, uri);

            // we are running in dual-pane mode - so we should NOT run animations
            bundle.putBoolean(DETAIL_TRANSITION_ANIMATION, false);

            // Create the detail fragment and add it to this activity using a fragment transaction
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(bundle);

            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.weather_detail_container, detailFragment, DETAILFRAGMENT_TAG)
                        .commit();
            }
        } else {
            mTwoPane = false;
        }

//        ForecastFragment forecastFragment =  ((ForecastFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_forecast));
//        getSupportActionBar().setElevation(0f);

//        if (getIntent().getExtras() != null) {
//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.d(LOG_TAG, "Key: " + key + " Value: " + value);
//            }
//        }

//        FirebaseMessaging.getInstance().subscribeToTopic("news");
//        String token = FirebaseInstanceId.getInstance().getToken();
//        Log.d(LOG_TAG, token);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String location = Utility.getPreferredLocation( this );
        // update the location in our second pane using the fragment manager
        if (location != null && !location.equals(mLocation)) {
            ForecastFragment ff = (ForecastFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
            if ( null != ff ) {
                mCurentDate = ff.getCurrentDateAsLong();
//                Log.v(LOG_TAG, "current date from static fragment is: " + mCurentDate);
            }
            DetailFragment df = (DetailFragment)getSupportFragmentManager().findFragmentByTag(DETAILFRAGMENT_TAG);
            if ( null != df ) {
                df.onLocationChanged(location, mCurentDate);
            }
            mLocation = location;
        }
    }

    @Override
    public void onItemSelected(Uri dateUri, ForecastAdapter.ViewHolder vh) {
        if (mTwoPane) {
            if (findViewById(R.id.weather_detail_container) != null) {
                // The detail container view will be present only in the large-screen layouts
                // (res/layout-sw600dp). If this view is present, then the activity should be
                // in two-pane mode.
                mTwoPane = true;
                // In two-pane mode, show the detail view in this activity by
                // adding or replacing the detail fragment using a
                // fragment transaction.

                Bundle bundle = new Bundle();

                // store the required uri for the DetailFragment to use
                bundle.putParcelable(DETAIL_URI, dateUri);

                // we are running in dual-pane mode - so we should NOT run animations
                bundle.putBoolean(DETAIL_TRANSITION_ANIMATION, false);

                // Create the detail fragment and add it to this activity using a fragment transaction
                DetailFragment detailFragment = new DetailFragment();
                detailFragment.setArguments(bundle);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.weather_detail_container, detailFragment, DETAILFRAGMENT_TAG)
                        .commit();

            }
        } else {
            Intent intent = new Intent(this, DetailActivity.class).setData(dateUri);
            ActivityOptionsCompat activityOptions = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, new Pair<View, String>(vh.iconImageView, getString(R.string.transition_forecast_to_detail)));
            ActivityCompat.startActivity(this, intent, activityOptions.toBundle());
        }
    }
}
