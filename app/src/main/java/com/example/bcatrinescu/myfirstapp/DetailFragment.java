package com.example.bcatrinescu.myfirstapp;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bcatrinescu.myfirstapp.data.WeatherContract;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bcatrinescu on 10.10.2016.
 */

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {
    private static final String LOG_TAG = DetailFragment.class.getSimpleName();

    public static final String DETAILFRAGMENT_TAG = "DFTAG";
    public static final String DETAIL_TRANSITION_ANIMATION = "DTA";
    public static final String DETAIL_URI = "URI";
    private static final int DETAIL_LOADER = 135156;
    private ShareActionProvider mShareActionProvider;
    private String mForecast;
    public static String weatherForTheDay;
    private static final String FORECAST_SHARE_HASHTAG = " #SunshineApp";
    private Uri mUri;
    private ViewHolder mViewHolder;
    private boolean mTransitionAnimation;

//        private static final String[] FORECAST_COLUMNS = {
//                WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
//                WeatherContract.WeatherEntry.COLUMN_DATE,
//                WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
//                WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
//                WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
//        };
//
//        // these constants correspond to the projection defined above, and must change if the
//        // projection changes
//        private static final int COL_WEATHER_ID = 0;
//        private static final int COL_WEATHER_DATE = 1;
//        private static final int COL_WEATHER_DESC = 2;
//        private static final int COL_WEATHER_MAX_TEMP = 3;
//        private static final int COL_WEATHER_MIN_TEMP = 4;


    public DetailFragment() { setHasOptionsMenu(true); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            mUri = arguments.getParcelable(DETAIL_URI);
            mTransitionAnimation = arguments.getBoolean(DETAIL_TRANSITION_ANIMATION);
        }

        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        /** Get the holder object containing each View id we are interested in, part of our layout */
        mViewHolder = new ViewHolder(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    private Intent createShareForecastIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mForecast + FORECAST_SHARE_HASHTAG);
        return shareIntent;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail, menu);
        MenuItem shareItem = menu.findItem(R.id.action_share_all_options);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        if (mForecast != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        } else {
//            Log.d(LOG_TAG,"Share Action Provider is null ?");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (null != mUri) {
//            Log.v(LOG_TAG, "Directly in the loader: - on create loader - : " + mUri);
            return new CursorLoader(getActivity(), mUri,
                    ForecastFragment.FORECAST_COLUMNS,
                    null,
                    null,
                    null
            );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (!cursor.moveToFirst()) {
            if (getView().getParent() instanceof CardView) {
                ((CardView) getView().getParent()).setVisibility(View.INVISIBLE);
            }
            return;
        }

        String iconId = cursor.getString(ForecastFragment.COL_ICON);
        int weatherId = cursor.getInt(ForecastFragment.COL_WEATHER_CONDITION_ID);
        String iconUrlAsString = Utility.getArtUrlForWeatherCondition(getContext(), weatherId, iconId);
        Uri forecastImageUri = Uri.parse(iconUrlAsString);
        Picasso.with(getContext()).load(forecastImageUri).into(mViewHolder.iconImageView);
//        Log.v(LOG_TAG, "TransitionName for imageView is:" + ViewCompat.getTransitionName(mViewHolder.iconImageView));

        // set the exact date textView
        SimpleDateFormat formatterNumericDay = ((SimpleDateFormat) DateFormat.getDateInstance()); // TODO: 28.11.2016  CAST MUST BE in TRY-CATCH
        formatterNumericDay.applyPattern("MMMM d");

        long dayInMills = cursor.getLong(ForecastFragment.COL_WEATHER_DATE);
        Date selectedDate = new Date(dayInMills);

//        String numericDay = formatterNumericDay.format(selectedDate);
//        (viewHolder.numericDayTextView).setText(numericDay);
        // done setting the exact date textView

        // set the day of the week
        SimpleDateFormat formatterWeekDay = ((SimpleDateFormat) DateFormat.getDateInstance()); // TODO: 28.11.2016  CAST MUST BE in TRY-CATCH
        formatterWeekDay.applyPattern("EEEE");
        String day = formatterWeekDay.format(selectedDate);
        (mViewHolder.dayTextView).setText(day);
        // done setting the day of the week

        // set title and
        FragmentActivity fragmentActivity = getActivity();
        if (fragmentActivity != null) {
            fragmentActivity.setTitle(getString(R.string.acc_details, day));
        }

        String forecast = cursor.getString(ForecastFragment.COL_WEATHER_DESC);
        (mViewHolder.forecastTextView).setText(forecast);

//        (viewHolder.iconForecastContainer).setContentDescription("Forecast: " + forecast);

        String highTemp = Utility.formatTemperature(getContext(), cursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP), Utility.isMetric(getContext()));
        (mViewHolder.maxTempTextView).setText(highTemp);

        String lowTemp = Utility.formatTemperature(getContext(), cursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP), Utility.isMetric(getContext()));
        (mViewHolder.minTempTextView).setText(lowTemp);

        int humidity = cursor.getInt(ForecastFragment.COL_HUMIDITY);
        (mViewHolder.humidityTextView).setText(getString(R.string.format_humidity, humidity));

        int pressure = cursor.getInt(ForecastFragment.COL_PRESSURE);
        (mViewHolder.pressureTextView).setText(getString(R.string.format_pressure, pressure));

        int windDegrees = cursor.getInt(ForecastFragment.COL_WIND_DEGREES);
//        (viewHolder.windView).setAngle(windDegrees);

        String windDirection = "";
        if (windDegrees < 30 || windDegrees > 330) windDirection += "N";
        else if (windDegrees < 60) windDirection += "NE";
        else if (windDegrees < 120) windDirection += "E";
        else if (windDegrees < 150) windDirection += "SE";
        else if (windDegrees < 210) windDirection += "S";
        else if (windDegrees < 240) windDirection += "SW";
        else if (windDegrees < 300) windDirection += "W";
        else if (windDegrees < 330) windDirection += "NW";
//        (viewHolder.windDegrees).setText(getString(R.string.format_wind_degrees, windDirection, windDegrees));

        String windspeed = cursor.getString(ForecastFragment.COL_WIND_SPEED);
        (mViewHolder.windTextView).setText(getString(R.string.format_wind_speed_and_direction, windspeed, windDirection));

        mForecast = day + " will be " + forecast + " " + highTemp + " / " + lowTemp;

        // If onCreateOptionsMenu has already happened, we need to update the share intent now.
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
        if (getView().getParent() instanceof CardView) {
            ((CardView) getView().getParent()).setVisibility(View.VISIBLE);
        }

        if (mTransitionAnimation) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.supportStartPostponedEnterTransition();
        }
//        Log.v(LOG_TAG, "Resume the animation");
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) { }

    void onLocationChanged( String newLocation, long dateAsLong) {
        mUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(newLocation, dateAsLong);
        getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
    }
    
    /** This static object will store the IDs of each of the available views. Use it in a view TAG */
    private class ViewHolder {
        public final ImageView iconImageView;
        public final TextView dayTextView;
//        public final TextView numericDayTextView;
        public final TextView minTempTextView;
        public final TextView maxTempTextView;
        public final TextView forecastTextView;
        public final TextView humidityTextView;
        public final TextView windTextView;
        public final TextView pressureTextView;
//        public final TextView windDegrees;
//        public final WindView windView;
//        public final FrameLayout iconForecastContainer;

        public ViewHolder(View v){
            iconImageView = (ImageView) v.findViewById(R.id.list_item_icon);
            dayTextView = (TextView) v.findViewById(R.id.list_item_date_textview);
//            numericDayTextView =  (TextView) v.findViewById(R.id.list_item_exact_date_textview2);
            minTempTextView = (TextView) v.findViewById(R.id.list_item_low_textview);
            maxTempTextView = (TextView) v.findViewById(R.id.list_item_high_textview);
            forecastTextView = (TextView) v.findViewById(R.id.list_item_forecast_textview);
            humidityTextView = (TextView) v.findViewById(R.id.list_item_humidity_textview);
            windTextView = (TextView) v.findViewById(R.id.list_item_wind_textview);
            pressureTextView = (TextView) v.findViewById(R.id.list_item_pressure_textview);
//            windDegrees = (TextView) v.findViewById(R.id.list_item_wind_degrees_textview);
//            windView = (WindView) v.findViewById(R.id.wind_view);
//            iconForecastContainer = (FrameLayout) v.findViewById(R.id.icon_forecast_container_details_fragment);
        }
    }
}
