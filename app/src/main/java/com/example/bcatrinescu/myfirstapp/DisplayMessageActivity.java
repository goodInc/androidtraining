package com.example.bcatrinescu.myfirstapp;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_display_message);

        Intent intent = getIntent();

        if (intent.getType().indexOf("image/") != -1) {
            Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
            ImageView imageView = (ImageView) findViewById(R.id.messageImage_imageView);
            // set dimension using dpi
//            imageView.setMinimumHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, getResources().getDisplayMetrics()));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setImageURI(imageUri);
        } else if (intent.getType().equals("text/plain")) {
            layout.removeView(findViewById(R.id.messageImage_imageView));
            String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
            TextView textView = new TextView(this);
            textView.setText(message);
            textView.setTextSize(20);
            layout.addView(textView);
        }
    }

    public static class TitlesFragment extends ListFragment {
        boolean mDualPane;
        int mCurCheckPosition = 0;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            // Populate list with our static array of titles
            setListAdapter(new ArrayAdapter<String>(getActivity(),
                    R.layout.simple_list_item_activated_1, Shakespeare.TITLES));

            // Check to see if we have a frame in which to embed the details fragment directly
            // in the containing UI
            View detailsFrame = getActivity().findViewById(R.id.details);
            mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

            // check if the Fragment existed at some point (to make it look as it looked before)
            if (savedInstanceState != null) {
                mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
            }

            if (mDualPane) {
                // in dual-pane mode, the list view highlights the selected item
                getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                // make sure our UI is in the correct state
                showDetails(mCurCheckPosition);
            }
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt("curChoice", mCurCheckPosition);
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            showDetails(position);
        }

        /** "helper" function to show details in a Fragment or in a new Activity */

        void showDetails(int index) {
            // the current position can be set by the user or it can be already set if the fragment
            // is restored
            mCurCheckPosition = index;

            if (mDualPane) {
                // We can display everything in place with fragments, so update the list and
                // highlight the selected item
                getListView().setItemChecked(index, true);

                // Check what fragment is currently shown, replace if needed
                DetailsFragment details = (DetailsFragment) getFragmentManager().findFragmentById(R.id.details);

                if (details == null || details.getShownIndex() != index) {
                    // make new fragment to show the selection
                    details = DetailsFragment.newInstance(index);

                    // execute a transaction, replacing any existing fragment with this one inside the frame
                    FragmentTransaction ft = getFragmentManager().beginTransaction();

                    ft.replace(R.id.details, details);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.commit();
                }
            }  else {
                // Otherwise we need to launch a new Activity to display the details
                Intent intent = new Intent();
                intent.setClass(getActivity(), OldDetailsActivity.class);
                intent.putExtra("index", index);
                startActivity(intent);
            }
        }
    }

    public static class DetailsFragment extends Fragment {

        public static DetailsFragment newInstance(int index) {
            DetailsFragment df = new DetailsFragment();
            Bundle args = new Bundle();
            args.putInt("index", index);
            df.setArguments(args);
            return df;
        }

        public int getShownIndex() {
            return getArguments().getInt("index", 0); // default value
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            if (container == null) {
                // we have different layouts and in one of them this fragment's containing frame
                // doesn't exist. The fragment may still be created from it's saved state, but there
                // is no reason to create its view hierarchy because it won't be displayed
                return null;
            }

            ScrollView scroller = new ScrollView(getActivity());
            TextView text = new TextView(getActivity());
            text.setPadding(3, 3, 3, 3);
            scroller.addView(text);
            text.setText(Shakespeare.DIALOGUE[getShownIndex()]);
            return scroller;
        }
    }
}