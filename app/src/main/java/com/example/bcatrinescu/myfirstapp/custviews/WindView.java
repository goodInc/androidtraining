package com.example.bcatrinescu.myfirstapp.custviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import com.example.bcatrinescu.myfirstapp.R;

import static android.R.attr.angle;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * TODO: document your custom view class.
 */
public class WindView extends View {
//    private String mExampleString; // TODO: use a default from R.string...
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...
//    private float mExampleDimension = 100;
//    private Drawable mCompassDrawable;
    private Paint mArrowPaint;
    private Paint mTextPaint;
    private double mArrowAngle;
    private String mSpokenWindDirection;
//    private AnimationDrawable mArrowDrawable;
//
//    private TextPaint mTextPaint;
//    private float mTextWidth;
//    private float mTextHeight;


    public WindView(Context context) {
        super(context);
        init(null, 0);
    }

    public WindView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public WindView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public void setAngle(int angle) {
        mArrowAngle = angle; // the API assigned 0 degrees to the north
    }

    private void init(AttributeSet attrs, int defStyle) {
        mArrowPaint = new Paint();

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.WindView, defStyle, 0);
//        mExampleString = a.getString(
//                R.styleable.WindView_exampleString);
//
//        /** Use getDimensionPixelSize or getDimensionPixelOffset when dealing with values that should fall on pixel boundaries. */
//        mExampleDimension = a.getDimension(
//                R.styleable.WindView_exampleDimension,
//                mExampleDimension);

//        if (a.hasValue(R.styleable.WindView_exampleDrawable)) {
//            mCompassDrawable = a.getDrawable(
//                    R.styleable.WindView_exampleDrawable);
//            mCompassDrawable.setCallback(this);
//        }
        mExampleColor = a.getColor(R.styleable.WindView_exampleColor, mExampleColor);
        mArrowPaint.setColor(mExampleColor);
        mArrowPaint.setStrokeWidth(10);
        mArrowPaint.setStyle(Paint.Style.STROKE); // makes the circle empty inside
//
//        if (a.hasValue(R.styleable.WindView_arrowDrawable)) {
//            mArrowDrawable = (AnimationDrawable) a.getDrawable(
//                    R.styleable.WindView_arrowDrawable);
//        }
//        this.setBackground(mArrowDrawable);
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(mExampleColor);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setTextSize(36);
//        if (mTextHeight == 0) {
//            mTextHeight = mTextPaint.getTextSize();
//        } else {
//            mTextPaint.setTextSize(mTextHeight);
//        }

        AccessibilityManager accessibilityManager =
                (AccessibilityManager) getContext().getSystemService(
                        Context.ACCESSIBILITY_SERVICE);
        if (accessibilityManager.isEnabled()) {
            sendAccessibilityEvent(
                    AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
        }

        a.recycle();
        // Set up a default TextPaint object
//        mTextPaint = new TextPaint();
//        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        mTextPaint.setTextAlign(Paint.Align.LEFT);

        // Update TextPaint and text measurements from attributes
//        invalidateTextPaintAndMeasurements();
    }
//    private void invalidateTextPaintAndMeasurements() {
//        mTextPaint.setTextSize(mExampleDimension);
//        mTextPaint.setColor(mExampleColor);
//        mTextWidth = mTextPaint.measureText(mExampleString);
//
//        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
//        mTextHeight = fontMetrics.bottom;
//    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

//        Log.v(VIEW_LOG_TAG, "view width " + contentWidth);
//        Log.v(VIEW_LOG_TAG, "view height " + contentHeight);

//        Log.v("WindView", String.format("Size of layout %d x %d", contentWidth, contentHeight));
//        // Draw the example drawable on top of the text.
//        if (mCompassDrawable != null) {
//            mCompassDrawable.setBounds(paddingLeft, paddingTop,
//                    paddingLeft + contentWidth, paddingTop + contentHeight);
//            mCompassDrawable.setAlpha(100);
//            mCompassDrawable.draw(canvas);
//        }
        /** Draw a line and rotate it (with the whole canvas, actually) */
//        canvas.save(); // this saves the current matrix to a private stack; later, it will be restored
        float startX = contentWidth / 2 + paddingLeft;
        float startY = contentHeight / 2 + paddingTop;
        double angleInRadians = (mArrowAngle-90) * Math.PI/180;
//        canvas.rotate(angle, contentWidth/2, contentHeight/2);
        int len = contentWidth / 2 - paddingRight;
        float stopX = (float) (len * cos(angleInRadians)) + startX;
        float stopY = (float) (len * sin(angleInRadians)) + startY;
        canvas.drawLine(startX, startY, stopX , stopY , mArrowPaint);

        /** draw the circle */
        canvas.drawCircle(startX,startY,len,mArrowPaint);

        /** draw the N, S, V, E pointers*/
        canvas.drawText("N", startX, paddingTop + 10, mTextPaint );
        canvas.drawText("E", contentWidth + paddingRight, startY, mTextPaint );
        canvas.drawText("S", startX, contentHeight + paddingBottom + 10, mTextPaint );
        canvas.drawText("V", paddingLeft, startY, mTextPaint );

        AccessibilityManager accessibilityManager =
                (AccessibilityManager) getContext().getSystemService(
                        Context.ACCESSIBILITY_SERVICE);
        if (accessibilityManager.isEnabled()) {
            sendAccessibilityEvent(
                    AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
        }

//        canvas.restore();
//
//        if (mArrowDrawable != null) {
//            mArrowDrawable.setBounds(contentWidth/ 2 - paddingLeft - 25, paddingTop, 350, 300);
//        }
//
//        // Draw the text.
//        canvas.drawText(mExampleString,
//                paddingLeft + (contentWidth - mTextWidth) / 2,
//                paddingTop + (contentHeight + mTextHeight) / 2,
//                mTextPaint);
    }

//    /**
//     * Gets the example string attribute value.
//     *
//     * @return The example string attribute value.
//     */
//    public String getExampleString() {
//        return mExampleString;
//    }
//
//    /**
//     * Sets the view's example string attribute value. In the example view, this string
//     * is the text to draw.
//     *
//     * @param exampleString The example string attribute value to use.
//     */
//    public void setExampleString(String exampleString) {
//        mExampleString = exampleString;
//        invalidateTextPaintAndMeasurements();
//    }

    @Override
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
        mSpokenWindDirection = "The wind is blowing ";
        if (mArrowAngle < 30 || mArrowAngle > 330) mSpokenWindDirection += "North";
        else if (mArrowAngle < 60) mSpokenWindDirection += "North East";
        else if (mArrowAngle < 120) mSpokenWindDirection += "East";
        else if (mArrowAngle < 150) mSpokenWindDirection += "South East";
        else if (mArrowAngle < 210) mSpokenWindDirection += "South";
        else if (mArrowAngle < 240) mSpokenWindDirection += "South Vest";
        else if (mArrowAngle < 300) mSpokenWindDirection += "Vest";
        else if (mArrowAngle < 330) mSpokenWindDirection += "North Vest";
        event.getText().add(mSpokenWindDirection);
//        Log.v(VIEW_LOG_TAG, "Arrow angle " + mArrowAngle);
        return true;
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getExampleColor() {
        return mExampleColor;
    }

    /**
     * Sets the view's example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */
    public void setExampleColor(int exampleColor) {
        mExampleColor = exampleColor;
//        invalidateTextPaintAndMeasurements();
    }
//
//    /**
//     * Gets the example dimension attribute value.
//     *
//     * @return The example dimension attribute value.
//     */
//    public float getExampleDimension() {
//        return mExampleDimension;
//    }
////
//    /**
//     * Sets the view's example dimension attribute value. In the example view, this dimension
//     * is the font size.
//     *
//     * @param exampleDimension The example dimension attribute value to use.
//     */
//    public void setExampleDimension(float exampleDimension) {
//        mExampleDimension = exampleDimension;
////        invalidateTextPaintAndMeasurements();
//    }
//
//    /**
//     * Gets the example drawable attribute value.
//     *
//     * @return The example drawable attribute value.
//     */
//    public Drawable getExampleDrawable() {
//        return mCompassDrawable;
//    }
//
//    /**
//     * Sets the view's example drawable attribute value. In the example view, this drawable is
//     * drawn above the text.
//     *
//     * @param exampleDrawable The example drawable attribute value to use.
//     */
//    public void setExampleDrawable(Drawable exampleDrawable) {
//        mCompassDrawable = exampleDrawable;
//    }
}
