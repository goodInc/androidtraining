package com.example.bcatrinescu.myfirstapp.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.bcatrinescu.myfirstapp.BuildConfig;
import com.example.bcatrinescu.myfirstapp.R;
import com.example.bcatrinescu.myfirstapp.Utility;
import com.example.bcatrinescu.myfirstapp.data.WeatherContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Created by bcatrinescu on 21.10.2016.
 */

public class SunshineService extends IntentService {
    private final String LOG_TAG = SunshineService.class.getSimpleName();
    public static final String LOCATION_QUERY_EXTRA = "lqe";
    private final int URL_CONNECT_TIMEOUT_MILS = 2000;
    private final int URL_READ_TIMEOUT_MILS = 3500;
    private SharedPreferences sharedPreferences;

    public SunshineService(){
        super("SunshineService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    /**
     * Invoked when you call startService(Intent)
     * @param intent will be put in a Queue and passed to a background thread (the code is defined in this "onHandleIntent(intent) method
     *               Sequentially, background threads will be created until all the Intents in the Queue will be done with.
     *               The service will be destroyed after all Intents were consumed from the Queue
     */
    protected void onHandleIntent(Intent intent) {
        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;
//            String[] prettyForecastJsonArray = null;
        Vector<ContentValues> prettyForecastJsonVector = null;

        Bundle extras = intent.getExtras(); // default value set via the XML in preferences file
        String location = extras.getString(LOCATION_QUERY_EXTRA);

//        Log.v(LOG_TAG, "ALIIVE IN THE SUNSHINE SERVICE background thread. :D!!! with location: " + location);

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily";
            final String QUERY_PARAM = "q";
            final String FORMAT_PARAM = "mode";
            final String UNITS_PARAM = "units";
            final String DAYS_PARAM = "cnt";
            final String APPID_PARAM = "APPID";
            /** set the default query parameters */
            String mode = "json";
            String units = "metric"; // default value set via the XML in preferences file
            String cnt = "7";

            Uri uri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(QUERY_PARAM, location).appendQueryParameter(FORMAT_PARAM, mode)
                    .appendQueryParameter(UNITS_PARAM, units).appendQueryParameter(DAYS_PARAM, cnt)
                    .appendQueryParameter(APPID_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();
            URL url = new URL(uri.toString());

            // google photos api
            // https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=park&rankby=prominence&key=
            // https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CoQBcwAAACSwqbux7si-2aoDgBaJVu29eEzP9uYQiAh2vWMLtDCAATq43ZkDZzvxYjj6RvkTCx_Dtfl8FZhn64NG-cQj7LTDuKuXoTiwn26GJqUz-x5rBZOyf5492JTrItFVCNRh4oGyiCorKhYkYrAiH_JQK700O4rqyHT_wlrjMI-fNXymEhAHGZ_Epy9kUoJZ4aU7G5mMGhSppsCTNuJmJnH0zwBetMRkjFs_FQ&key=
            // https://developers.google.com/places/web-service/search#PlaceSearchRequests

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(URL_CONNECT_TIMEOUT_MILS);
            urlConnection.setReadTimeout(URL_READ_TIMEOUT_MILS);
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }
            forecastJsonStr = buffer.toString();

//                prettyForecastJsonArray = getWeatherDataFromJson(forecastJsonStr, 7);
            prettyForecastJsonVector = getWeatherDataFromJson(forecastJsonStr, location, 7);
//            Log.v(LOG_TAG, String.format("+++ We should have weather for %d days to show", prettyForecastJsonVector.size()));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return;
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Error ", e);
            e.printStackTrace();
            // malformed json string or something else. no point in continuing
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        if (prettyForecastJsonVector == null) {
//                Log.w(LOG_TAG, "The ARRAY seems to be null!");
            return;
        }

        ContentValues[] contentValuesAsArray = new ContentValues[prettyForecastJsonVector.size()];
        int resultOfDBInsert = this.getContentResolver().bulkInsert(WeatherContract.WeatherEntry.CONTENT_URI, prettyForecastJsonVector.toArray(contentValuesAsArray));
//        Log.v(LOG_TAG, String.format("done updating the data with the Sunshine background service with %d days worth of data", contentValuesAsArray.length));

        /** In the end, return the result to display */
//        return prettyForecastJsonVector;
    }

    private Vector<ContentValues> getWeatherDataFromJson(String forecastJsonStr, String location, int numDays)
            throws JSONException {

        // These are the names of the JSON objects that need to be extracted.
        final String OWM_LIST = "list";
        final String OWM_CITY = "city";
        final String OWM_CITY_NAME = "name";
        final String OWM_COORD = "coord";
        final String OWM_LATITUDE = "lat";
        final String OWM_LONGITUDE = "lon";
        final String OWM_WEATHER = "weather";
        final String OWM_TEMPERATURE = "temp";
        final String OWM_MAX = "max";
        final String OWM_MIN = "min";
        final String OWM_DESCRIPTION = "main";
        final String OWM_ICON = "icon";
        final String OWM_PRESSURE = "pressure";
        final String OWM_HUMIDITY = "humidity";
        final String OWM_WINDSPEED = "speed";
        final String OWM_WIND_DIRECTION = "deg";
        final String OWM_WEATHER_ID = "id";

        /** Get the root json object containing all weather and location information */
        JSONObject forecastJson = new JSONObject(forecastJsonStr);

        /** Get the json object containing the location information */
        JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
        String cityName = cityJson.getString(OWM_CITY_NAME);

        JSONObject cityCoord = cityJson.getJSONObject(OWM_COORD);
        double cityLatitude = cityCoord.getDouble(OWM_LATITUDE);
        double cityLongitude = cityCoord.getDouble(OWM_LONGITUDE);

        long locationId = addLocation(location, cityName, cityLatitude, cityLongitude);

        /** Get the object containing only the weather information for the number of days requested */
        JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);

        // OWM returns daily fragment_forecast based upon the local time of the city that is being
        // asked for, which means that we need to know the GMT offset to translate this data
        // properly.

        // Since this data is also sent in-order and the first day is always the
        // current day, we're going to take advantage of that to get a nice
        // normalized UTC date for all of our weather.
//
//            Time dayTime = new Time();
//            dayTime.setToNow();
//
//            // we start at the day returned by local time. Otherwise this is a mess.
//            int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);
//
//            // now we work exclusively in UTC
//            dayTime = new Time();

        List<Map<String, String>> resultListOfDaysAsMaps = new ArrayList<>();

        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        SimpleDateFormat formatterToday = ((SimpleDateFormat) DateFormat.getDateInstance());
        formatterToday.applyPattern("MMMM d");
        SimpleDateFormat formatterWeekDay = ((SimpleDateFormat) DateFormat.getDateInstance());
        formatterWeekDay.applyPattern("EEEE");
        SimpleDateFormat formatterExactDay = ((SimpleDateFormat) DateFormat.getDateInstance());
        formatterExactDay.applyPattern("EEE, MMM d");

        Vector<ContentValues> contentValuesVector = new Vector<>(weatherArray.length());

        for (int i = 0; i < weatherArray.length(); i++, calendar.add(Calendar.DATE, 1)) {
            String day;
            String description;
            String icon;
            long dateTime = calendar.getTimeInMillis();
            double pressure;
            int humidity;
            double windSpeed;
            double windDirection;
            int weatherId;

            // Get the JSON object representing the day
            JSONObject dayForecast = weatherArray.getJSONObject(i);
            /** Prepare the map for the SimpleAdapter; this will represent a "row" with a date */
            Map<String, String> resultMap = new HashMap<>();

            if (i == 0) {
                day = getString(R.string.loc_today, formatterToday.format(calendar.getTime()));
            } else if (i == 1) {
                day = getString(R.string.loc_tomorrow);
            } else if (i < 7) {
                day = formatterWeekDay.format(calendar.getTime()).toUpperCase();
            } else {
                day = formatterExactDay.format(calendar.getTime()).toUpperCase();
            }

            // description is in a child array called "weather", which is 1 element long.
            JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
            description = weatherObject.getString(OWM_DESCRIPTION);
            icon = weatherObject.getString(OWM_ICON);
//                Log.v(LOG_TAG,"The data icon is: " + icon);

            // Temperatures are in a child object called "temp".  Try not to name variables
            // "temp" when working with temperature.  It confuses everybody.
            JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);

            weatherId = weatherObject.getInt(OWM_WEATHER_ID);
            double high = temperatureObject.getDouble(OWM_MAX);
            double low = temperatureObject.getDouble(OWM_MIN);
//            Log.v(LOG_TAG, String.format("\t\t%s\t\tmax %.1f min %.1f \t%s", location, high, low, day));

            /** format the temperatures, from Metric to anything you want (currently, Imperial is supported) */
            /** we assume that all data is metric and we convert it on the fly */
            String unitsPrefered = sharedPreferences.getString(this.getString(R.string.pref_units_key), this.getString(R.string.pref_weather_units_default)); // this should not be empty!
            boolean isMetric = unitsPrefered.compareTo(this.getString(R.string.pref_weather_units_default)) == 0 ? true : false; // return "true" if the user set "metric"

            String formattedHighTemp = Utility.formatTemperature(this, high, isMetric);
            String formattedLowTemp = Utility.formatTemperature(this, low, isMetric);

            pressure = dayForecast.getDouble(OWM_PRESSURE);
            humidity = dayForecast.getInt(OWM_HUMIDITY);
            windSpeed = dayForecast.getDouble(OWM_WINDSPEED);
            windDirection = dayForecast.getDouble(OWM_WIND_DIRECTION);

            ContentValues weatherValues = new ContentValues();

            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_LOC_KEY, locationId);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DATE, dateTime);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_NATURAL_DATE, day);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, windDirection);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, high);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, low);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC, description);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WEATHER_CONDITION_ID, weatherId);
            weatherValues.put(WeatherContract.WeatherEntry.COLUMN_ICON, icon);

            contentValuesVector.add(weatherValues);

            resultMap.put("list_item_date_textview", day);
            resultMap.put("list_item_forecast_textview", description);
            /** don't hardcode the formatting as below ! instead use a format defined as a string, get it from the context etc... */
                /*resultMap.put("list_item_high_textview", Long.toString(highLowResult[0]) + " " + unitsDegree);*/
            resultMap.put("list_item_high_textview", formattedHighTemp);
            resultMap.put("list_item_low_textview", formattedLowTemp);

                /* Integer.toString(R.drawable.clouds_and_sun)  */
//                RequestCreator rc = Picasso.with(getContext()).load("http://openweathermap.org/img/w/02n.png").placeholder(R.drawable.clouds_and_sun);


            resultMap.put("list_item_icon", icon);
            resultListOfDaysAsMaps.add(resultMap);
        }
        return contentValuesVector;
    }

    long addLocation(String locationSetting, String cityName, double lat, double lon) {
        // Students: First, check if the location with this city name exists in the db
        // If it exists, return the current ID
        // Otherwise, insert it using the content resolver and the base URI

        ContentValues contentValues = new ContentValues();
        contentValues.put(WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,locationSetting);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_CITY_NAME,cityName);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, lat);
        contentValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, lon);

        // A cursor is your primary interface to the query results.
        Cursor cursor = this.getContentResolver().query(
                WeatherContract.LocationEntry.CONTENT_URI,
                null,
                WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + "=?",
                new String[]{locationSetting},
                null);


        if (cursor.moveToFirst() == false) {
            Uri uri = this.getContentResolver().insert(WeatherContract.LocationEntry.CONTENT_URI, contentValues);
//            Log.v(LOG_TAG, "+++++ addLocation called with location id and name: " + Long.parseLong(uri.getLastPathSegment()) + " " + locationSetting + " new LOCATION inserted");
            return Long.parseLong(uri.getLastPathSegment());
        } else {
//            Log.v(LOG_TAG, "+++++ addLocation called with location id and name: " + cursor.getLong(cursor.getColumnIndex(WeatherContract.LocationEntry._ID)) + " " + locationSetting + " LOCATION already exists");
            return cursor.getLong(cursor.getColumnIndex(WeatherContract.LocationEntry._ID));
        }
    }

    public static class AlarmReceiver extends BroadcastReceiver {
        private static final String LOG_TAG = AlarmReceiver.class.getSimpleName();
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(LOCATION_QUERY_EXTRA)) {
                // get the location from the recieved intent (should be present as an "extra")
                String location = intent.getStringExtra(LOCATION_QUERY_EXTRA);
//                Log.v(LOG_TAG, "PendingIntent received with location: " + location);
                // create a new Intent, put the location in it and start a service with it
                Intent sunshineServiceIntent = new Intent(context, SunshineService.class);
                sunshineServiceIntent.putExtra(LOCATION_QUERY_EXTRA, location);
                context.startService(sunshineServiceIntent);
            }
        }
    }
}
