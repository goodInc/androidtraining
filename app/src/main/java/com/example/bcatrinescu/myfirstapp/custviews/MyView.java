package com.example.bcatrinescu.myfirstapp.custviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by bcatrinescu on 17.10.2016.
 */

public class MyView extends View {
    private static final String LOG_TAG = MyView.class.getSimpleName();
    private Paint mTextPaint;
    private float mTextWidth = 0.0f;
    private float mTextHeight = 26.0f;
    private int mTextColor = Color.MAGENTA;

    private Paint mCirclePaint;
    private float mPointerRadius = 122.0f;
    private float mPointerX;
    private float mPointerY;
    private float mPointerStrokeWidth = 5.0f;
    private String mText = "Custom View";


    public MyView(Context context) {
        super(context);
        init();
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // find out which "mode"
        int hSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        // get the size
        int hSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        int myHeight = hSpecSize;

        if (hSpecMode == MeasureSpec.EXACTLY) {
            myHeight = hSpecSize;
        } else if (hSpecMode == MeasureSpec.AT_MOST) {
            // wrap content
        }

        /** Do the same thing for width */

        // find out which "mode"
        int wSpecMode = MeasureSpec.getMode(heightMeasureSpec);
        // get the size
        int wSpecSize = MeasureSpec.getSize(heightMeasureSpec);
        int myWidth = wSpecSize;

        if (wSpecMode == MeasureSpec.EXACTLY) {
            myWidth = wSpecSize;
        } else if (wSpecMode == MeasureSpec.AT_MOST) {
            // wrap content
        }
//        Log.v(LOG_TAG, "the measured width and height for MyView: " + myWidth + " " + myHeight);
        setMeasuredDimension(myWidth, myHeight);
    }

    private void init(){
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(mTextColor);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        if (mTextHeight == 0) {
            mTextHeight = mTextPaint.getTextSize();
        } else {
            mTextPaint.setTextSize(mTextHeight);
        }

        mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCirclePaint.setColor(Color.BLUE);
        mCirclePaint.setStyle(Paint.Style.STROKE); // makes the circle empty inside
        mCirclePaint.setStrokeWidth(mPointerStrokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPointerX = getHeight() / 2;
        mPointerY = getWidth()  / 2;
        mPointerRadius = (mPointerX < mPointerY ? mPointerX : mPointerY) - mPointerStrokeWidth;
//        Log.v(LOG_TAG, "the parameters for MyView: " + mPointerY + " " + mPointerX + " " + mPointerRadius);
        canvas.drawCircle(getHeight()/2,getWidth()/2,mPointerRadius,mCirclePaint);

        canvas.drawText(mText, mPointerRadius + mPointerStrokeWidth, mPointerY, mTextPaint);
    }
}
